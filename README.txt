CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Information
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Current Maintainer: lexsoft <https://www.drupal.org/u/lexsoft>

The main dotMailer module provides an API for interacting with dotMailer. Once you have
 added your API account details your users will be able to sign up to address
 books within dotMailer.

INFORMATION
------------

This module contains a sub-module dotmailer_newsletter.

REQUIREMENTS
------------

This module requires the following library:
 * Dotmailer-php (https://packagist.org/packages/drupalcoders/dotmailer-php).

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
     https://www.drupal.org/docs/7/extend/installing-modules
     for further information.
 * Go to the root of the module and do a 'composer install' to install the third party
     library

CONFIGURATION
-------------

 * Setup your dotMailer account details at:
  admin/config/services/dotmailer-service/account_details
 * Have a test to check if the API is working at:
  admin/config/services/dotmailer-service/account_details/test
