<?php

/**
 * @file
 * Testing functionality for dotmailer.
 */

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

/**
 * The DotMailer test form.
 *
 * @param array $form
 *   The current contents of the form.
 * @param array $form_submit
 *   The current submission of the form.
 *
 * @return array
 *   The form.
 */
function dotmailer_api_dotmailer_test(array $form, array $form_submit) {
  $form['dotmailer_test'] = [
    '#type' => 'fieldset',
    '#title' => 'DotMailer account test',
    '#submit' => ['dotmailer_api_dotmailer_test_submit'],
  ];

  $username = variable_get('dotmailer_api_username', '');

  if ($username == '' && $username == '') {
    $help_text = t('In order to run a test you need to enter your DotMailer credentials');
  }
  else {
    $help_text = t('Click the button below to initiate a test on the DotMailer API using the credentials you have entered.');
  }

  $form['dotmailer_test']['submit_test_help'] = [
    '#markup' => '<p>' . $help_text . '</p>',
  ];

  $form['dotmailer_test']['test_email'] = [
    '#type' => 'textfield',
    '#title' => t('Test email address'),
  ];

  $form['dotmailer_test']['test_options'] = [
    '#type' => 'checkboxes',
    '#title' => t('Test Options'),
    '#options' => [
      'get_address_books' => t('Get address books'),
      'get_contact_address_books' => t('Get contact address books'),
      'unsubscribe' => t('Unsubscribe contact'),
      'resubscribe' => t('Resubscribe contact'),
      'delete' => t('Delete contact'),
    ],
    '#required' => TRUE,
  ];

  $form['dotmailer_test']['submit_test'] = [
    '#type' => 'submit',
    '#value' => 'Test DotMailer API',
  ];

  if ($username == '' && $username == '') {
    $form['dotmailer_test']['submit_test']['#disabled'] = TRUE;
  }

  return $form;
}

/**
 * Submit function for dotmailer_api_dotmailer_test().
 *
 * Run the DotMailer api test.
 *
 * @param array $form
 *   The current contents of the form.
 * @param array $form_state
 *   The form state.
 *
 * @return bool
 *   True if the test passed. Otherwise false.
 */
function dotmailer_api_dotmailer_test_submit(array $form, array $form_state) {

  $dotmailer = dotmailer_api_get_object();

  $email = $form_state['values']['test_email'];
  $test_options = $form_state['values']['test_options'];

  $contact = NULL;
  $message = t('Please select a test option');
  if (!empty($email)) {
    try {
      $contact = $dotmailer->getContactByEmail($email);
    }
    catch (ClientException $e) {
      $message = $e->getMessage();
    }
    catch (RequestException $e) {
      $message = $e->getMessage();
    }
    catch (Exception $e) {
      $message = $e->getMessage();
    }
    if ($contact) {
      $message = t('The following account:');
      $message .= '<ul>';
      $message .= '<li>' . $contact->getId() . '</li>';
      $message .= '<li>' . $contact->getEmail() . '</li>';
      $message .= '<li>' . $contact->getOptInType() . '</li>';
      $message .= '<li>' . $contact->getEmailType() . '</li>';
      $message .= '<li>' . print_r($contact->getDataFields()) . '</li>';
      $message .= '</ul>';
      foreach ($test_options as $option) {
        if ($option !== 0) {
          switch ($option) {
            case 'get_contact_address_books':
              $contact_addressbooks = NULL;
              try {
                $contact_addressbooks = $dotmailer->getContactAddressBooks($contact);
              }
              catch (ClientException $e) {
                $message = $e->getMessage();
              }
              catch (RequestException $e) {
                $message = $e->getMessage();
              }
              catch (Exception $e) {
                $message = $e->getMessage();
              }

              if (is_array($contact_addressbooks) && count($contact_addressbooks) > 0) {
                $message .= t('You have the following address books in your DotMailer account');

                $message .= '<ul>';
                foreach ($contact_addressbooks as $address_book) {
                  $message .= '<li>' . $address_book->getName() . '</li>';
                }
                $message .= '</ul>';

              }
              break;

            case 'unsubscribe':
              try {
                $dotmailer->unsubscribeContact($contact);
              }
              catch (ClientException $e) {
                $message = $e->getMessage();
              }
              catch (RequestException $e) {
                $message = $e->getMessage();
              }
              catch (Exception $e) {
                $message = $e->getMessage();
              }
              $message .= t('You have unsubscribed the following contact: @name', ['@name' => $contact->getEmail()]);
              break;

            case 'resubscribe':
              try {
                $dotmailer->resubscribeContact($contact);
              }
              catch (ClientException $e) {
                $message = $e->getMessage();
              }
              catch (RequestException $e) {
                $message = $e->getMessage();
              }
              catch (Exception $e) {
                $message = $e->getMessage();
              }
              $message .= t('You have resubscribed the following contact: @name', ['@name' => $contact->getEmail()]);
              break;

            case 'delete':
              try {
                $dotmailer->deleteContact($contact);
              }
              catch (ClientException $e) {
                $message = $e->getMessage();
              }
              catch (RequestException $e) {
                $message = $e->getMessage();
              }
              catch (Exception $e) {
                $message = $e->getMessage();
              }
              $message .= t('You have deleted the following contact: @name', ['@name' => $contact->getEmail()]);
              break;
          }
        }

      }
    }
    drupal_set_message(filter_xss($message), 'status');
    return TRUE;
  }
  elseif ($test_options['get_address_books'] !== 0) {

    try {
      $contact_addressbooks = $dotmailer->getAddressBooks();
    }
    catch (ClientException $e) {
      $message = $e->getMessage();
    }
    catch (RequestException $e) {
      $message = $e->getMessage();
    }
    catch (Exception $e) {
      $message = $e->getMessage();
    }

    if (is_array($contact_addressbooks) && count($contact_addressbooks) > 0) {
      $message .= t('You have the following address books in your DotMailer account');

      $message .= '<ul>';
      foreach ($contact_addressbooks as $address_book) {
        $message .= '<li>' . $address_book->getName() . '</li>';
      }
      $message .= '</ul>';

    }
    else {
      $message = t('You do not have any address books in your DotMailer account');
    }

    drupal_set_message(filter_xss($message), 'status');
    return TRUE;
  }
  else {
    $message = t('Please enter an email address');
    drupal_set_message(filter_xss($message), 'status');
    return TRUE;
  }
}
