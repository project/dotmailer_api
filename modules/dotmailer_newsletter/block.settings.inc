<?php

/**
 * @file
 * Contains dotMailer block settings.
 */

const FIRST_NAME = 'FIRSTNAME';
const LAST_NAME = 'LASTNAME';
const COUNTRY = 'COUNTRY';
const COMPANY = 'COMPANY';

/**
 * DotMailer default block settings.
 *
 * @param string $block_id
 *   Block ID Delta.
 *
 * @return array
 *   The form.
 */
function dotmailer_block_settings_form($block_id) {

  $default_settings = [
    'selected_address_book' => [],
    'option_type' => t('Single'),
    'first_name_include' => 0,
    'first_name_field' => FIRST_NAME,
    'last_name_include' => 0,
    'last_name_field' => LAST_NAME,
    'country_include' => 0,
    'country_field' => COUNTRY,
    'company_include' => 0,
    'company_field' => COMPANY,
    'confirmation_text_field' => [
      'value' => t('Thank you for registering to receive updates from @site_name.<br> To complete your subscription you must confirm your email address.',
        ['@site_name' => variable_get('site_name', 'Drupal')]
      ),
      'format' => 'filtered_html',
    ],
    'honeypot_include' => 0,
    'ajax_include' => 0,
    'request_logs' => 0,
    'request_errors' => 0,
  ];

  $honeypot_exists = module_exists('honeypot');

  $settings = variable_get('dotmailer_newsletter_settings_' . $block_id, []);

  $settings = array_merge($default_settings, $settings);

  $dotmailer = dotmailer_api_get_object();

  if ($dotmailer === FALSE) {
    $form['dotmailer_newsletter_error'] = [
      '#type' => 'markup',
      '#markup' => '<p class="error">' . t('An error occurred when getting dotMailer object, please check api settings.') . '</p>',
    ];
    return $form;
  }

  $address_book_objects = $dotmailer->getAddressBooks();
  $address_books = [];

  if ($address_book_objects !== FALSE) {
    foreach ($address_book_objects as $address_book) {
      $address_books[$address_book->getId()] = $address_book->getName();
    }
  }

  return [
    'selected_address_book' => [
      '#type' => 'checkboxes',
      '#title' => t('Pick one or more address book:'),
      '#options' => $address_books,
      '#required' => TRUE,
      '#default_value' => $settings['selected_address_book'],
    ],
    'option_type' => [
      '#type' => 'select',
      '#title' => t('Select opt in type:'),
      '#options' => [
        'Single' => 'Single',
        'Double' => 'Double',
        'VerifiedDouble' => 'Verified double',
      ],
      '#required' => TRUE,
      '#default_value' => $settings['option_type'],
      '#description' => t('When setting up verified double opt in dotMailer you can also 
          set the opt in redirect page to be "node/in-my-website".'),
    ],
    'name_field_set' => [
      '#type' => 'fieldset',
      '#title' => t('Name field'),
      'first_name_include' => [
        '#type' => 'checkbox',
        '#title' => t('Include first name field?'),
        '#default_value' => $settings['first_name_include'],
      ],
      'first_name_field' => [
        '#type' => 'textfield',
        '#title' => t('First Name field dotMailer id'),
        '#default_value' => $settings['first_name_field'],
        '#states' => [
          'invisible' => [
            ':input[name="first_name_include"]' => ['checked' => FALSE],
          ],
        ],
      ],
      'last_name_include' => [
        '#type' => 'checkbox',
        '#title' => t('Include last name field?'),
        '#default_value' => $settings['last_name_include'],
      ],
      'last_name_field' => [
        '#type' => 'textfield',
        '#title' => t('Last Name field dotMailer id'),
        '#default_value' => $settings['last_name_field'],
        '#states' => [
          'invisible' => [
            ':input[name="last_name_include"]' => ['checked' => FALSE],
          ],
        ],
      ],
      'country_include' => [
        '#type' => 'checkbox',
        '#title' => t('Include country field?'),
        '#default_value' => $settings['country_include'],
      ],
      'country_field' => [
        '#type' => 'textfield',
        '#title' => t('Country field dotMailer id'),
        '#default_value' => $settings['country_field'],
        '#states' => [
          'invisible' => [
            ':input[name="country_include"]' => ['checked' => FALSE],
          ],
        ],
      ],
      'company_include' => [
        '#type' => 'checkbox',
        '#title' => t('Include company field?'),
        '#default_value' => $settings['company_include'],
      ],
      'company_field' => [
        '#type' => 'textfield',
        '#title' => t('Company field dotMailer id'),
        '#default_value' => $settings['company_field'],
        '#states' => [
          'invisible' => [
            ':input[name="company_include"]' => ['checked' => FALSE],
          ],
        ],
      ],
    ],

    'confirmation_text' => [
      '#type' => 'fieldset',
      '#title' => t('Text field'),
      'confirmation_text_field' => [
        '#type' => 'text_format',
        '#title' => t('Confirmation message.'),
        '#default_value' => $settings['confirmation_text_field']['value'],
        '#format' => $settings['confirmation_text_field']['format'],
        '#resizable' => TRUE,
        '#required' => TRUE,
      ],
    ],
    'honeypot_include' => [
      '#type' => 'checkbox',
      '#title' => t('Include Honeypot protection?'),
      '#disabled' => !$honeypot_exists,
      '#default_value' => $settings['honeypot_include'],
      '#description' => t('You need to have <a href="https://www.drupal.org/project/honeypot">Honeypot</a> installed!'),
    ],
    'ajax_include' => [
      '#type' => 'checkbox',
      '#title' => t('Enable Ajax'),
      '#default_value' => $settings['ajax_include'],
    ],
    'watchdog_logs' => [
      '#type' => 'fieldset',
      '#title' => t('Watchdog logs'),
      'request_logs' => [
        '#type' => 'checkbox',
        '#title' => t('Enable requests logs'),
        '#default_value' => $settings['request_logs'],
      ],
      'request_errors' => [
        '#type' => 'checkbox',
        '#title' => t('Enable request errors'),
        '#default_value' => $settings['request_errors'],
      ],
    ],

  ];
}
