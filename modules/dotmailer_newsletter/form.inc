<?php

/**
 * @file
 * Contains the signup form and ajax handler.
 */

use Dotmailer\Entity\Contact;
use Dotmailer\Entity\AddressBook;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

/**
 * Create a signup form for dotMailer signups.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 *
 * @return array
 *   The form.
 */
function dotmailer_newsletter_form(array $form, array &$form_state, $block_id) {

  $form['#attributes'] = ['class' => 'dotmailer-newsletter-form'];

  $default_settings = [
    'selected_address_book' => [],
    'audience_type' => t('Unknown'),
    'option_type' => t('Single'),
    'first_name_include' => 0,
    'first_name_field' => FIRST_NAME,
    'last_name_include' => 0,
    'last_name_field' => LAST_NAME,
    'country_include' => 0,
    'country_field' => COUNTRY,
    'company_include' => 0,
    'company_field' => COMPANY,
    'confirmation_text_field' => [
      'value' => t('Thank you for registering to receive updates from @site_name.<br> To complete your subscription you must confirm your email address.', ['@site_name' => variable_get('site_name', 'Drupal')]),
      'format' => 'filtered_html',
    ],
    'honeypot_include' => 0,
    'ajax_include' => 0,
  ];

  $settings = variable_get('dotmailer_newsletter_settings_' . $block_id, []);
  $settings = array_merge($default_settings, $settings);

  $form_state['dotmailer_newsletter_settings'] = $settings;

  $form['error_container'] = [
    '#prefix' => '<div id="dotmailer_newsletter_form_error_container">',
    '#markup' => '<!-- Comment -->',
    '#suffix' => '</div>',
  ];

  $form['#prefix'] = '<div id="dotmailer_newsletter_form_wrapper" >';
  $form['#suffix'] = '</div>';

  if ($settings['first_name_include']) {
    // Only show the name field if previously selected.
    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => t('Your First Name'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Your First Name',
        'required' => 'required',
      ],
    ];
  }

  if ($settings['last_name_include']) {
    // Only show the name field if previously selected.
    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Your Last Name'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Your Last Name',
        'required' => 'required',
      ],
    ];
  }

  if ($settings['country_include']) {
    // Only show the name field if previously selected.
    $form['country'] = [
      '#type' => 'textfield',
      '#title' => t('Your Country'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Your Country',
      ],
    ];
  }

  if ($settings['company_include']) {
    // Only show the name field if previously selected.
    $form['company'] = [
      '#type' => 'textfield',
      '#title' => t('Your Company'),
      '#attributes' => [
        'placeholder' => 'Your Company',
      ],
    ];
  }

  $form['email'] = [
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#required' => TRUE,
    '#attributes' => [
      'placeholder' => 'Email Address',
      'required' => 'required',
    ],
  ];

  if (count($settings['selected_address_book_with_values']) > 1) {
    $form['addressbook'] = [
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#title' => t('CHOOSE A NEWSLETTER'),
      '#options' => $settings['selected_address_book_with_values'],
    ];
  }
  $form['subscribe'] = [
    '#type' => 'submit',
    '#value' => t('Subscribe'),
    '#attributes' => [
      'class' => ['btn', 'btn-primary'],
    ],
    '#prefix' => '<div id="form-actions" class="form-actions">',
    '#markup' => '<!-- Comment -->',
    '#suffix' => '</div>',

  ];
  if ($settings['ajax_include']) {
    $form['subscribe']['#ajax'] = [
      'callback' => 'dotmailer_newsletter_form_ajax_handler',
      'effect' => 'fade',
    ];
  }

  // Check if Honeypot module exists and add form protection.
  if (module_exists('honeypot') && $settings['honeypot_include']) {
    honeypot_add_form_protection($form, $form_state, [
      'honeypot',
      'time_restriction',
    ]);
  }

  return $form;
}

/**
 * Implements hook_FORM_ID_validate().
 *
 * Ensure e-mail is valid.
 */
function dotmailer_newsletter_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['email']) && !filter_var($form_state['values']['email'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('email', t("The email address you entered is not valid. Please check and try again."));
  }
}

/**
 * Implements hook_FORM_ID_submit().
 *
 * @throws \Drupal\dotmailer_api\DotMailer\Exception\MissingRequiredParametersException
 */
function dotmailer_newsletter_form_submit($form, &$form_state) {
  $form_state['dotmailer_newsletter_error'] = FALSE;
  // Connect to Dotmailer Integration API.
  $dotmailer_api = dotmailer_api_get_object();

  if ($dotmailer_api === FALSE) {
    watchdog('dotMailer signup subscription error: no dotMailer object found', 'error');
    drupal_set_message(t('An error occurred whilst processing your signup request, please try again later.'), 'error');
    return FALSE;
  }

  $settings = $form_state['dotmailer_newsletter_settings'];
  $expected_fields = ['first_name', 'last_name', 'country', 'company'];

  $fields = [];
  foreach ($expected_fields as $setting_field) {
    $value = trim($form_state['values'][$setting_field]);
    if (!empty($value) && $settings[$setting_field . '_include'] && !empty($settings[$setting_field . '_field'])) {
      $fields[$settings[$setting_field . '_field']] = $value;
    }
  }
  $email = $form_state['values']['email'];

  if (empty($form_state['values']['addressbook'])) {
    $form_state['values']['addressbook'] = array_keys(array_filter($settings['selected_address_book']));
  }

  // Check if user exists within dotMailer.
  $dotmailer_user_exists = FALSE;
  try {
    $dotmailer_user_exists = $dotmailer_api->GetContactByEmail($email);
    if ($settings['request_logs'] == TRUE) {
      watchdog('dotmailer', 'Log message - GetContactByEmail: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($dotmailer_user_exists, TRUE)], WATCHDOG_INFO);
    }
  }
  catch (ClientException $e) {
    $message = $e->getMessage();
    // Log the exception to watchdog.
    if ($settings['request_errors'] == TRUE) {
      watchdog('dotmailer', 'ClientException - GetContactByEmail: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
    }
  }
  catch (RequestException $e) {
    $message = $e->getMessage();
    // Log the exception to watchdog.
    if ($settings['request_errors'] == TRUE) {
      watchdog('dotmailer', 'RequestException - GetContactByEmail: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
    }
  }
  catch (Exception $e) {
    $message = $e->getMessage();
    // Log the exception to watchdog.
    if ($settings['request_errors'] == TRUE) {
      watchdog_exception('dotmailer', $message);
    }
  }

  // If user exists, check if they are already in the address book.
  if ($dotmailer_user_exists) {
    foreach ($fields as $key => $field) {
      $dotmailer_user_exists->setDataField($key, $field);
    }
    $dotmailer_user_exists->setOptInType($settings['option_type']);

    // Get address books that the contact is subscribed to.
    $dotmailer_contacts_address_books = NULL;
    try {
      $dotmailer_contacts_address_books = $dotmailer_api->getContactAddressBooks($dotmailer_user_exists);
      if ($settings['request_logs'] == TRUE) {
        watchdog('dotmailer', 'Log message - getContactAddressBooks: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($dotmailer_contacts_address_books, TRUE)], WATCHDOG_INFO);
      }
    }
    catch (ClientException $e) {
      $message = $e->getMessage();
      // Log the exception to watchdog.
      if ($settings['request_errors'] == TRUE) {
        watchdog('dotmailer', 'ClientException - getContactAddressBooks: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
      }
    }
    catch (RequestException $e) {
      $message = $e->getMessage();
      // Log the exception to watchdog.
      if ($settings['request_errors'] == TRUE) {
        watchdog('dotmailer', 'RequestException - getContactAddressBooks: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
      }
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      // Log the exception to watchdog.
      if ($settings['request_errors'] == TRUE) {
        watchdog_exception('dotmailer', $message);
      }
    }
    if ($dotmailer_contacts_address_books !== NULL) {
      if (!empty($dotmailer_contacts_address_books)) {
        foreach ($dotmailer_contacts_address_books as $address_book) {
          foreach ($form_state['values']['addressbook'] as $address_book_id) {
            if ($address_book_id > 0 && $address_book->getId() !== $address_book_id) {
              $address_book_object = new AddressBook($address_book_id, $address_book_id);
              try {
                $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($dotmailer_user_exists, $address_book_object);
                if ($settings['request_logs'] == TRUE) {
                  watchdog('dotmailer', 'Log message - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($dotmailer_user_to_address_book, TRUE)], WATCHDOG_INFO);
                }
              }
              catch (ClientException $e) {
                $message = $e->getMessage();
                if ($settings['request_errors'] == TRUE) {
                  watchdog('dotmailer', 'ClientException - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
                }
              }
              catch (RequestException $e) {
                $message = $e->getMessage();
                if ($settings['request_errors'] == TRUE) {
                  watchdog('dotmailer', 'RequestException - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
                }
              }
              catch (Exception $e) {
                $message = $e->getMessage();
                if ($settings['request_errors'] == TRUE) {
                  watchdog_exception('dotmailer', $message);
                }
              }
            }
          }
        }
      }
      else {
        $dotmailer_address_books = $dotmailer_api->getAddressBooks();
        foreach ($dotmailer_address_books as $address_book) {
          foreach ($form_state['values']['addressbook'] as $address_book_id) {
            if ($address_book_id > 0 && $address_book->getId() == $address_book_id) {
              $address_book_object = new AddressBook($address_book_id, $address_book_id);
              try {
                $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($dotmailer_user_exists, $address_book_object);
                if ($settings['request_logs'] == TRUE) {
                  watchdog('dotmailer', 'Log message - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($dotmailer_user_to_address_book, TRUE)], WATCHDOG_INFO);
                }
              }
              catch (ClientException $e) {
                $message = $e->getMessage();
                if ($settings['request_errors'] == TRUE) {
                  watchdog('dotmailer', 'ClientException - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
                }
              }
              catch (RequestException $e) {
                $message = $e->getMessage();
                if ($settings['request_errors'] == TRUE) {
                  watchdog('dotmailer', 'RequestException - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
                }
              }
              catch (Exception $e) {
                $message = $e->getMessage();
                if ($settings['request_errors'] == TRUE) {
                  watchdog_exception('dotmailer', $message);
                }
              }
            }
          }
        }
      }
    }
    else {
      $dotmailer_address_books = $dotmailer_api->getAddressBooks();
      foreach ($dotmailer_address_books as $address_book) {
        foreach ($form_state['values']['addressbook'] as $address_book_id) {
          if ($address_book_id > 0 && $address_book->getId() == $address_book_id) {
            try {
              $dotmailer_user_to_address_book = $dotmailer_api->resubscribeContactToAddressBook($dotmailer_user_exists, $address_book);
              if ($settings['request_logs'] == TRUE) {
                watchdog('dotmailer', 'Log message - resubscribeContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($dotmailer_user_to_address_book, TRUE)], WATCHDOG_INFO);
              }
            }
            catch (ClientException $e) {
              $message = $e->getMessage();
              if ($settings['request_errors'] == TRUE) {
                watchdog('dotmailer', 'ClientException - resubscribeContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
              }
            }
            catch (RequestException $e) {
              $message = $e->getMessage();
              if ($settings['request_errors'] == TRUE) {
                watchdog('dotmailer', 'RequestException - resubscribeContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
              }
            }
            catch (Exception $e) {
              $message = $e->getMessage();
              if ($settings['request_errors'] == TRUE) {
                watchdog_exception('dotmailer', $message);
              }
            }
          }
        }
      }

    }
  }
  else {
    $new_contact = new Contact(NULL, $email, $settings['option_type'], 'Html');

    foreach ($fields as $key => $field) {
      $new_contact->setDataField($key, $field);
    }

    // $contact = $dotmailer_api->createContact($new_contact);
    $dotmailer_address_books = $dotmailer_api->getAddressBooks();
    foreach ($dotmailer_address_books as $address_book) {
      foreach ($form_state['values']['addressbook'] as $address_book_id) {
        if ($address_book_id > 0 && $address_book->getId() == $address_book_id) {
          try {
            $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($new_contact, $address_book);
            if ($settings['request_logs'] == TRUE) {
              watchdog('dotmailer', 'Log message - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($dotmailer_user_to_address_book, TRUE)], WATCHDOG_INFO);
            }
          }
          catch (ClientException $e) {
            $message = $e->getMessage();
            // Log the exception to watchdog.
            if ($settings['request_errors'] == TRUE) {
              watchdog('dotmailer', 'ClientException - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
            }
          }
          catch (RequestException $e) {
            $message = $e->getMessage();
            // Log the exception to watchdog.
            if ($settings['request_errors'] == TRUE) {
              watchdog('dotmailer', 'RequestException - AddContactToAddressBook: <pre>@placeholder_name</pre>', ['@placeholder_name' => print_r($message, TRUE)], WATCHDOG_NOTICE);
            }
          }
          catch (Exception $e) {
            $message = $e->getMessage();
            // Log the exception to watchdog.
            if ($settings['request_errors'] == TRUE) {
              watchdog_exception('dotmailer', $message);
            }
          }
        }
      }
    }
  }
  if (!$settings['ajax_include']) {
    // Redirect the user to thank you page.
    $form_state['redirect'] = 'newsletter-signup/thank-you';
  }

}

/**
 * Implements hook_FORM_ID_submit().
 *
 * @throws Exception
 */
function dotmailer_newsletter_form_ajax_handler($form, &$form_state) {
  $commands = [];

  if (!empty(form_get_errors())) {
    $messages = theme('status_messages');
    $commands[] = ajax_command_html('#dotmailer_newsletter_form_error_container', $messages);
  }
  else {
    $settings = $form_state['dotmailer_newsletter_settings'];
    $page_body = $settings['confirmation_text_field'];
    $commands[] = ajax_command_replace('#dotmailer_newsletter_form_wrapper', '<div id="messages">' . check_markup($page_body['value'], $page_body['format']) . '</div>');
  }

  // 3) Return our ajax commands.
  return ['#type' => 'ajax', '#commands' => $commands];

}
