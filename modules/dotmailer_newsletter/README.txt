CONTENTS OF THIS FILE
---------------------

 * Information
 * Requirements
 * Installation
 * Configuration

INFORMATION
------------

This is a sub-module dotmailer_newsletter.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
     https://www.drupal.org/docs/7/extend/installing-modules
     for further information.

CONFIGURATION
-------------

 * This generates a custom 'dotMailer Newsletter Signup' block:
  admin/structure/block/manage/dotmailer_newsletter/dotmailer_newsletter_block/configure
